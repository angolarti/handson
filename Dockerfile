FROM node:lts

WORKDIR /app
COPY package.json .
RUN npm i -g npm@7.18.1 \
    && npm i

COPY . .
RUN chown -R 1000:1000 .
USER 1000

CMD [ "node", "index.js" ]